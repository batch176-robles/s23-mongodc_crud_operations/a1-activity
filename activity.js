db.users.insertMany([
{
    "firstname": "James",
    "lastname": "Howlett",
    "email": "wolverine@mail.com",
    "password": "wolverine123",
    "isAdmin": false
},
{
    "firstname": "Jean",
    "lastname": "Grey",
    "email": "jeangrey@mail.com",
    "password": "jeangrey123",
    "isAdmin": false
},
{
    "firstname": "Ororo",
    "lastname": "Munroe",
    "email": "storm@mail.com",
    "password": "storm123",
    "isAdmin": false
},{
    "firstname": "Scott",
    "lastname": "Summers",
    "email": "cyclopes@mail.com",
    "password": "cyclops123",
    "isAdmin": false
},{
    "firstname": "Charles",
    "lastname": "Xavier",
    "email": "professorx@mail.com",
    "password": "professorx123",
    "isAdmin": false
}
])

db.courses.insertMany([
{
    "name": "Mutant History 101",
    "price": 1000,
    "isActive": false
},
{
    "name": "Combat 101",
    "price": 2000,
    "isActive": false
},
{
    "name": "Special Powers 101",
    "price": 3000,
    "isActive": false
}
])

db.users.find({isAdmin:false})
db.users.updateOne({}, {$set:{"isAdmin": true}})
db.courses.updateOne({"name":"Special Powers 101"}, {$set:{"isActive": true}})
db.courses.deleteMany({"isActive": false})

